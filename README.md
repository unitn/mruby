mruby for Linino
================

This is an early version of mruby that can be cross-compiled for Linino and run on the Arduino Yùn.

Build Instructions
------------------

Set up a build environment as instructed on [http://playground.arduino.cc/Hardware/Yun#build_linino](Arduino Yùn FAQ page).

Export some environment vars:

    $ export STAGING_DIR="/home/yourpath/linino/trunk/staging_dir/target-mips_r2_uClibc-0.9.33.2"
    $ export PATH="$PATH:/home/yourpath/linino/trunk/staging_dir/toolchain-mips_r2_gcc-4.6-linaro_uClibc-0.9.33.2/bin"
    
Clone the linino branch:

    $ cd Your/dev/path
    $ git clone -b linino https://bitbucket.org/unitn/mruby.git

Move to your custom mruby dir and type rake. Binaries will be created in build/Atheros/bin directory.

Hack and anjoy.

