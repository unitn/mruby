MRuby::Gem::Specification.new('mruby-bin-indra') do |spec|
  spec.license = 'MIT'
  spec.author  = 'Paolo Bosetti'

  spec.linker.libraries << 'readline' if spec.cc.defines.include? "ENABLE_READLINE"

  spec.bins = %w(indra)
end
