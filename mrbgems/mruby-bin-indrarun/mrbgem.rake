MRuby::Gem::Specification.new('mruby-bin-indrarun') do |spec|
  spec.license = 'MIT'
  spec.author  = 'Paolo Bosetti'
  spec.bins = %w(indrarun)
end
