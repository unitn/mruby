MRuby::Gem::Specification.new('mruby-bin-mirb') do |spec|
  spec.license = 'MIT'
  spec.author  = 'mruby developers'

  spec.linker.libraries += %w(readline ncurses) if spec.cc.defines.include? "ENABLE_READLINE"

  spec.bins = %w(mirb)
end
