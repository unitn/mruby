MRuby::Gem::Specification.new('mruby-indramachine') do |spec|
  spec.license = 'MIT'
  spec.author  = 'Paolo Bosetti'
  
  spec.linker.flags = "-v"
  spec.linker.flags_before_libraries = "-stdlib=libc++"
  spec.cc.include_paths << "#{build.root}/../src"
  spec.linker.library_paths << "#{build.root}/../lib"
  spec.linker.libraries << %w(stdc++ c++abi EasyIndra)
end
