##
# EasyIndra interface
#
class IndraDrive
  attr_accessor :ip, :id, :control_mode
  def initialize(params = {})
    @ip           = (params[:ip] || "0.0.0.0")
    @id           = (params[:id] || 1)
    @control_mode = (params[:control_mode] || DRIVE_CONTROL_POSITION)
  end
end

class IndraMachine
  attr_reader :axes
  def initialize
    @axes = []
  end
  
  def <<(axis)
    if axis.kind_of? IndraDrive then
      if @axes.size > 0 && axis.ip == "0.0.0.0" #default
        octs = @axes.last.ip.split('.').map {|e| e.to_i}
        octs[-1] += 1
        axis.ip = octs.join('.')
      end
      @axes << axis
    else
      raise ArgumentError, "Need an IndraDrive object!"
    end
  end
  
  def count; @axes.size; end
end