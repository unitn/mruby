/*
** easyindra.c - EasyIndra interface functions
**
** (C)2013 Paolo Bosetti, UniTN
*/

#include <stdio.h>
#include <string.h>
#include "mruby.h"
#include "mruby/array.h"
#include "mruby/string.h"
#include "mruby/variable.h"

#include <EasyIndra.h>

#define CHECK_RV(v) (v) == 0 ? mrb_true_value() : mrb_false_value()


static mrb_value
mrb_indramachine_test(mrb_state *mrb, mrb_value self)
{
  printf("This is just a test!\n");
  return CHECK_RV(1);
}

/*
  CONFIGURATION
*/
static mrb_value
mrb_indramachine_Initialize(mrb_state *mrb, mrb_value self)
{
  int count = mrb_fixnum(mrb_funcall(mrb, self, "count", 0));
  EASYINDRA_DRIVE_PARAM params[count];
  EASYINDRA_RETVAL rv = 0;
  int axis_id         = 1;
  int i;
  mrb_value axes, axis, ip, id, control_mode;
  
  axes = mrb_iv_get(mrb, self, mrb_intern_cstr(mrb, "@axes"));
  for (i = 0; i < count; i++) {
    axis = mrb_ary_entry(axes, i);
    ip           = mrb_iv_get(mrb, axis, mrb_intern_cstr(mrb, "@ip"));
    id           = mrb_iv_get(mrb, axis, mrb_intern_cstr(mrb, "@id"));
    control_mode = mrb_iv_get(mrb, axis, mrb_intern_cstr(mrb, "@control_mode"));
    printf("Connect ID: %2d, CTRL: %1d, IP: %s\n", 
      mrb_fixnum(id),
      mrb_fixnum(control_mode), 
      mrb_string_value_cstr(mrb, &ip));    
    strcpy(params[i].ip, mrb_string_value_cstr(mrb, &ip));
    params[i].id           = mrb_fixnum(id);
    params[i].control_mode = mrb_fixnum(control_mode);
  }
  rv = EasyIndra_Initialize(&params, axis_id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_Shutdown(mrb_state *mrb, mrb_value self)
{
  EasyIndra_Shutdown();
  return mrb_nil_value();
}

// DRIVE ENABLING/DISABLING
static mrb_value
mrb_indramachine_EnableDrive(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  mrb_int axis_id;
  mrb_get_args(mrb, "i", &axis_id);
  rv = EasyIndra_EnableDrive((unsigned char)axis_id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_EnableAllDrives(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  rv = EasyIndra_EnableAllDrives();
  return CHECK_RV(rv);
}


static mrb_value
mrb_indramachine_DisableDrive(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  mrb_int axis_id;
  mrb_get_args(mrb, "i", &axis_id);
  rv = EasyIndra_DisableDrive((unsigned char)axis_id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_DisableAllDrives(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  rv = EasyIndra_DisableAllDrives();
  return CHECK_RV(rv);
}


static mrb_value
mrb_indramachine_StartDriveControl(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  mrb_int axis_id;
  mrb_get_args(mrb, "i", &axis_id);
  rv = EasyIndra_StartDriveControl(axis_id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_StartAllDriveControls(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  rv = EasyIndra_StartAllDriveControls();
  return CHECK_RV(rv);
}


static mrb_value
mrb_indramachine_StopDriveControl(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  mrb_int axis_id;
  mrb_get_args(mrb, "i", &axis_id);
  rv = EasyIndra_StopDriveControl(axis_id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_StopAllDriveControls(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  rv = EasyIndra_StopAllDriveControls();
  return CHECK_RV(rv);
}


static mrb_value
mrb_indramachine_ResetAllDrives(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  rv = EasyIndra_ResetAllDrives();
  return CHECK_RV(rv);
}


// DRIVE CONTROL
static mrb_value
mrb_indramachine_SetTarget(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  double target;
  unsigned char id;
  mrb_get_args(mrb, "fi", &target, &id);
  rv = EasyIndra_SetTarget(target, id);
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_SetTargets(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  int n_target, i;
  mrb_value *target;
  mrb_value axes;
  axes = mrb_iv_get(mrb, self, mrb_intern_cstr(mrb, "@axes"));
  mrb_get_args(mrb, "a", &target, &n_target);
  if (n_target != mrb_ary_len(mrb, axes))
    mrb_raise(mrb, E_ARGUMENT_ERROR, "Wrong array size");
  for (i=0; i<n_target; i++) {
    if (!mrb_nil_p(target[i])) {
      if (mrb_float_p(target[i])) {
        rv += EasyIndra_SetTarget(mrb_float(target[i]), i);
      } 
      else if (mrb_fixnum_p(target[i])) {
        rv += EasyIndra_SetTarget(mrb_fixnum(target[i]) * 1.0, i);
      }
    }
  }
  return CHECK_RV(rv);
}


// DRIVE INSPECTION
static mrb_value
mrb_indramachine_SampleDriveNow(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_SampleAllDrivesNow(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetPositions_Now(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetVelocities_Now(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetAccelerations_Now(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetTorques_Now(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}


static mrb_value
mrb_indramachine_StartMonitoringPVAT(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_StopMonitoring(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetPositions(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetVelocities(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetAccelerations(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}

static mrb_value
mrb_indramachine_GetTorques(mrb_state *mrb, mrb_value self)
{
  EASYINDRA_RETVAL rv = 0;
  printf("Method unimplemented!\n");
  return CHECK_RV(rv);
}



/*
  ERROR MANAGEMENT
*/
static mrb_value
mrb_indramachine_HasError(mrb_state *mrb, mrb_value self)
{
  return HasError() ? mrb_true_value() : mrb_false_value();
}

static mrb_value
mrb_indramachine_GetLastErrorMsg(mrb_state *mrb, mrb_value self)
{
  char error_msg[128];
  int  max_length = 128;
  bzero(error_msg, max_length);
  mrb_sym reset;
  mrb_get_args(mrb, "|n", &reset);
  EasyIndra_GetLastErrorMsg(error_msg, max_length);
  if (reset != mrb_intern_cstr(mrb, "keep"))
    EasyIndra_ResetError();
  return mrb_str_new_cstr(mrb, error_msg);
}

static mrb_value
mrb_indramachine_ResetError(mrb_state *mrb, mrb_value self)
{
  EasyIndra_ResetError();
  return mrb_nil_value();
}


/*
  MRUBY SETUP
*/

#define ENUM_TO_CONST(k,c,e) mrb_define_const(mrb, k, c, mrb_fixnum_value(e))
#define STRING_TO_CONST(k,c,s) mrb_define_const(mrb, k, c, mrb_str_new_cstr(mrb, s))
#define FLOAT_TO_CONST(k,c,f) mrb_define_const(mrb, k, c, mrb_float_value(f))

void
mrb_mruby_indramachine_gem_init(mrb_state* mrb)
{
  struct RClass *IndraMachine, *IndraDrive;

  IndraMachine = mrb_define_class(mrb, "IndraMachine", mrb->object_class);
  IndraDrive   = mrb_define_class(mrb, "IndraDrive",   mrb->object_class);

  STRING_TO_CONST(IndraMachine, "VERSION", "1.0");
  
  ENUM_TO_CONST(IndraDrive, "DRIVE_CONTROL_POSITION", EASYINDRA_DRIVE_CONTROL_POSITION);
  ENUM_TO_CONST(IndraDrive, "DRIVE_CONTROL_VELOCITY", EASYINDRA_DRIVE_CONTROL_VELOCITY);
  ENUM_TO_CONST(IndraDrive, "DRIVE_CONTROL_TORQUE",   EASYINDRA_DRIVE_CONTROL_TORQUE);

  mrb_define_method(mrb, IndraMachine, "test", mrb_indramachine_test, ARGS_NONE());

  mrb_define_method(mrb, IndraMachine, "connect",  mrb_indramachine_Initialize, ARGS_NONE());
  mrb_define_method(mrb, IndraMachine, "shutdown", mrb_indramachine_Shutdown,   ARGS_NONE());

  mrb_define_method(mrb, IndraMachine, "enable_drive",       mrb_indramachine_EnableDrive,      ARGS_REQ(1));
  mrb_define_method(mrb, IndraMachine, "enable_all_drives",  mrb_indramachine_EnableAllDrives,  ARGS_NONE());
  mrb_define_method(mrb, IndraMachine, "disable_drive",      mrb_indramachine_DisableDrive,     ARGS_REQ(1));
  mrb_define_method(mrb, IndraMachine, "disable_all_drives", mrb_indramachine_DisableAllDrives, ARGS_NONE());

  mrb_define_method(mrb, IndraMachine, "start_drive",      mrb_indramachine_StartDriveControl,     ARGS_REQ(1));
  mrb_define_method(mrb, IndraMachine, "start_all_drives", mrb_indramachine_StartAllDriveControls, ARGS_NONE());
  mrb_define_method(mrb, IndraMachine, "stop_drive",       mrb_indramachine_StopDriveControl,      ARGS_REQ(1));
  mrb_define_method(mrb, IndraMachine, "stop_all_drives",  mrb_indramachine_StopAllDriveControls,  ARGS_NONE());

  mrb_define_method(mrb, IndraMachine, "reset_all_drives", mrb_indramachine_ResetAllDrives, ARGS_NONE());

  mrb_define_method(mrb, IndraMachine, "set_target",  mrb_indramachine_SetTarget,  ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "set_targets", mrb_indramachine_SetTargets, ARGS_REQ(2));

  mrb_define_method(mrb, IndraMachine, "sample_drive",          mrb_indramachine_SampleDriveNow,       ARGS_REQ(3));
  mrb_define_method(mrb, IndraMachine, "sample_drives",         mrb_indramachine_SampleAllDrivesNow,   ARGS_REQ(3));
  mrb_define_method(mrb, IndraMachine, "get_positions_now",     mrb_indramachine_GetPositions_Now,     ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_velocities_now",    mrb_indramachine_GetVelocities_Now,    ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_accelerations_now", mrb_indramachine_GetAccelerations_Now, ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_torques_now",       mrb_indramachine_GetTorques_Now,       ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "start_monitoring",      mrb_indramachine_StartMonitoringPVAT,  ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "stop_monitoring",       mrb_indramachine_StopMonitoring,       ARGS_NONE());
  mrb_define_method(mrb, IndraMachine, "get_positions",         mrb_indramachine_GetPositions,         ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_velocities",        mrb_indramachine_GetVelocities,        ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_accelerations",     mrb_indramachine_GetAccelerations,     ARGS_REQ(2));
  mrb_define_method(mrb, IndraMachine, "get_torques",           mrb_indramachine_GetTorques,           ARGS_REQ(2));

  mrb_define_method(mrb, IndraMachine, "error?",        mrb_indramachine_HasError,        ARGS_NONE());
  mrb_define_method(mrb, IndraMachine, "error_message", mrb_indramachine_GetLastErrorMsg, ARGS_OPT(1));
  mrb_define_method(mrb, IndraMachine, "reset_error",   mrb_indramachine_ResetError,      ARGS_NONE());
}

void
mrb_mruby_indramachine_gem_final(mrb_state* mrb)
{
  printf("Shutting down connection\n");
  EasyIndra_Shutdown();
}
