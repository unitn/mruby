MRuby::Gem::Specification.new('mruby-yun') do |spec|
  spec.license = 'MIT'
  spec.author  = 'Paolo Bosetti'

  spec.linker.libraries += %w(readline ncurses) if spec.cc.defines.include? "ENABLE_READLINE"

  spec.bins = %w(mbridge)
end
