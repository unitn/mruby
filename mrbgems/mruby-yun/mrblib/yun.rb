##
# Arduino Yùn console
#

module Yun
  def self.reset_mcu
    `reset-mcu`
  end
  
  class Console
    PORT = 6571
    attr_reader :console
    
    def initialize
      @console = TCPSocket.new('localhost', PORT)
      if block_given?
        yield self
        self.close
      end
    end
    
    def close;    @console.close;           end
    def write(s); @console.write(s);        end
    def puts(s);  @console.write(s + "\n"); end
    def read;     @console.read;            end
    
  end
  
  class Bridge
    PORT = 5700
    def initialize
      @sok = TCPSocket.new('localhost', PORT)
    end
  
    def read_nonblock
      @sok._setnonblock(true)
      buf = ''
      while 1
        begin
          buf += @sok.recv(1)
        rescue => e
          break if buf.length > 0
        end
      end
      @sok._setnonblock(false)
      return buf
    end
  
    def put(key, value)
      msg = JSON::stringify({
        command:'put', 
        key:key, 
        value:value
      })
      @sok = TCPSocket.new('localhost', PORT)
      @sok.write msg
      response = self.read_nonblock
      @sok.close
      return JSON::parse response
    end
  
    def get(key)
      msg = JSON::stringify({
        command:'get', 
        key:key
      })
      @sok = TCPSocket.new('localhost', PORT)
      @sok.write msg
      response = self.read_nonblock
      @sok.close
      return JSON::parse response
    end
  end

end
