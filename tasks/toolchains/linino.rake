MRuby::Toolchain.new(:linino) do |conf|
  [conf.cc, conf.cxx, conf.objc, conf.asm].each do |cc|
    cc.command = ENV['CC'] || 'mips-openwrt-linux-uclibc-gcc'
    cc.flags = [ENV['CFLAGS'] || %w(-g -O3 -Wall -Werror-implicit-function-declaration)]
    cc.include_paths = [
    "#{MRUBY_ROOT}/include", 
    "#{ENV['STAGING_DIR']}/usr/include"
    ]
    cc.defines = %w(DISABLE_GEMS)
    cc.option_include_path = '-I%s'
    cc.option_define = '-D%s'
    cc.compile_options = '%{flags} -MMD -o %{outfile} -c %{infile}'
  end
  
  conf.archiver do |ar|
    ar.command = 'mips-openwrt-linux-uclibc-ar'
  end

  conf.linker do |linker|
    linker.command = ENV['LD'] || 'mips-openwrt-linux-uclibc-gcc'
    linker.flags = [ENV['LDFLAGS'] || %w()]
    linker.libraries = %w(c m)
    linker.library_paths = ["#{ENV['STAGING_DIR']}/usr/lib"]
    linker.option_library = '-l%s'
    linker.option_library_path = '-L%s'
    linker.link_options = '%{flags} -o %{outfile} %{objs} %{flags_before_libraries} %{libs} %{flags_after_libraries}'
  end
end
